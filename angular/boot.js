// main config for requirejs
require.config({
    waitSeconds: 20,
    //lets stop the caching
    urlArgs: "random=" + Math.random(),
    appDir: "./",
    paths: {
        "angular": '/node_modules/angular/angular.min',
        "angular-ui-bootstrap": '/node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls',
        "jquery": '/node_modules/jquery/dist/jquery.min',
        "jqueryUi": '/node_modules/jquery-ui-dist/jquery-ui.min',
        "spectrum": '/node_modules/spectrum-colorpicker/spectrum',
        "bootstrap": '/node_modules/bootstrap/dist/js/bootstrap.bundle.min',
        "three": '/node_modules/three/build/three',
        "trackball": '/node_modules/three/examples/js/controls/TrackballControls',
        "chosen": '/node_modules/chosen-js/chosen.jquery.min',

        // our stuff
        "ym": '/js/ym',
        "apis": '/js/apis',
        "rows": '/js/rows',
        "stacks": '/js/stacks',
        "threeD": '/js/threeD',
        "tooltip": '/js/tooltip',
        "zones": '/js/zones',
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        "angular-ui-bootstrap": {
            deps: ['angular']
        },
        'three': {
            exports: 'THREE'
        },
        'trackball': {
            deps: ['three']
        },
        'chosen': {
            deps: ['jquery']
        },
        "yard-management.module": {
            deps: ['angular']
        },
    },
    map: {
        '*': {
            three: 'three-glue'
        },
        'three-glue': {
            three: 'three'
        }
    }
});

require(['yard-management.module'], function () {
    console.log(angular);
    var $html = angular.element(document.getElementsByTagName('html'));
    angular.element().ready(function () {
        angular.bootstrap(document, ['yard-management']);
    });
})

define("three-glue", ["three"], function (three) {
    window.THREE = three;
    return three;
});
