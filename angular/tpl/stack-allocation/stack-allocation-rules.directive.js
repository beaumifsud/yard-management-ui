angular.module('yard-management')
.directive('stackAllocationRules', function(YardManagementService) {
    var directive = {};

    directive.templateUrl = "/angular/tpl/stack-allocation/stack-allocation-rules.html";
    directive.replace = false;

    directive.link = function(scope, $elem, attr) {
    };

    return directive;

});