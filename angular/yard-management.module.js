
angular.module('yard-management', ['ui.bootstrap'])
.value('yardGridSize', 25)
.factory('selectedContainer', function(){
    var selectedContainer = null;
    return {
        set: function(attributes){
            selectedContainer = {
                id : attributes.id,
                orientation : attributes.orientation,
                cols : parseInt(attributes.cols),
                rows : parseInt(attributes.rows),
                tiers : parseInt(attributes.tiers),
                size : parseInt(attributes.size)
            };
        },
        get: function(){
            return selectedContainer;
        }
    }
})