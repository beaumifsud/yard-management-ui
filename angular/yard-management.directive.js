angular.module('yard-management')
.directive('yardManagement', function(YardManagementService) {
    var directive = {};

    directive.templateUrl = "/angular/tpl/yard-management.controller.html";
    directive.replace = true;

    directive.link = function(scope, $elem, attr) {
        console.log($elem[0]);
        YardManagementService.init($elem[0]);
    };

    return directive;
});