var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var colors      = require('colors');


// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("./scss/*.scss", ['sass']);
    gulp.watch("./**/*.html").on('change', browserSync.reload);
});


// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("./scss/*.scss")
    .pipe(sass()).on('error', function(err){
        console.log(colors.bgRed('################################################'));
        console.log(colors.bgRed('################################################'));
        console.log(colors.bgRed(' '));
        console.log(colors.bgRed('################ ERRO : ' + err.messageOriginal));
        console.log(colors.bgRed('################ FILE : ' + err.relativePath));
        console.log(colors.bgRed('################ LINE : ' + err.line));
        console.log(colors.bgRed('################ COLU : ' + err.column));
        console.log(colors.bgRed(' '));
        console.log(colors.bgRed('################################################'));
        console.log(colors.bgRed('################################################'));
        this.emit('end');
    })
    .pipe(gulp.dest("./css"))
    .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);