angular.module('yard-management')
.factory('yardManagementApi', function($http) {

    function getStacks(userid){
        return $http.get("/js/stub/stacks.json?user="+userid)
        .then(function(httpresponse){return httpresponse.data});
    }
    
    function getZones(userid){
        return $http.get("/js/stub/zones.json?user="+userid)
        .then(function(httpresponse){return httpresponse.data});
    }

    function saveZone(zoneinfo){
        return $http.post("/js/stub/savezone/", zoneinfo)
        .then(function(httpresponse){return httpresponse.data});
    }

    function tooltip(colInfo){
        return $http.get("/js/stub/hover.json?id="+colInfo.id+"&row="+colInfo.row+"&col="+colInfo.col)
        .then(function(httpresponse){return httpresponse.data});
    }

    function rowInfo(rowNumber, stackId){
        return $http.get("/js/stub/row-info.json?row="+rowNumber+"&id="+stackId)
        .then(function(httpresponse){return httpresponse.data});
    }
    
    function newPosition(id,top,left, width,height){
        return $http.post("/js/stub/set-position/?id="+id+"&width="+width+"&height="+height+"&top="+top+"&left="+left)
        .then(function(httpresponse){return httpresponse.data});
    }

    function search(search){
        return $http.get("/js/stub/search.json?search="+search)
        .then(function(httpresponse){return httpresponse.data});
    }

    function containerInfo(){

    }

    return {
        getStacks : getStacks,
        getZones : getZones,
        tooltip : tooltip,
        rowInfo : rowInfo,
        search : search,
        containerInfo : containerInfo,
        saveZone : saveZone,
        newPosition: newPosition
    }
})