angular.module('yard-management')
.factory('rows', function(yardManagementApi, selectedContainer) {

    function doAnimation(row){
        var stack = row.closest('.stack');
        for (i=0; i < row.data('row'); i++) {
            $('.stack-row',stack).eq(i).addClass('row-selected');
        }

    }

    function loadRows(rowNumber) {

        var _this = this;
        var containerHTML = "";   
        
        // stop circular dependencies
        var stackInfo = selectedContainer.get();         

        yardManagementApi.rowInfo(rowNumber, stackInfo.id)
        .then(function(data){
            $.each(data,function(i,v){
                var dataObj = this;
                containerHTML = "<div class='container-row'>";
                $.each(dataObj.tier,function(ii,vv){
                    if (this.empty) {
                        containerHTML = containerHTML +"<div class='container-doors container-blank' data-id="+stackInfo.id+" data-row="+rowNumber+" data-stack="+(stackInfo.tiers - i)+" data-col="+ii+"></div>";
                    } else {
                        containerHTML = containerHTML + "<div class='container-doors container-img-"+Math.floor(Math.random() * 4 + 1)+"' data-id="+stackInfo.id+" data-row="+rowNumber+" data-stack="+(stackInfo.tiers - i)+" data-col="+ii+"><span>"+this.containerNumber+" <br> "+this.ISOCode+"</span></div>";
                    }
                });
                containerHTML = containerHTML + "</div>";
                $(containerHTML).appendTo('.row-information');
            })
        });  

        setTimeout(function(){

            $(".container-doors").not('.container-blank').draggable({
                
                revert: function(dropValid){                
                    if(!dropValid){
                        $('.row-view').removeClass('temp-hide');
                        $('.black-out').show();
                        hideDropZone();
                        return true;
                    }
                },
                drag: function( event, ui ) {

                    showDropZone();
                    var dragOffset = $(this).offset().top;
                    var rowInfoOffset = $('.row-view').offset().top;

                    if ((dragOffset + 50) < (rowInfoOffset - 100)) {
                        $('.row-view').addClass('temp-hide');
                        $('.black-out').hide();
                    } else {
                        $('.row-view').removeClass('temp-hide');
                        $('.black-out').show();
                    }
                }
            });

            $(".container-blank, .stack-container").droppable({
                hoverClass: "drop-hover",
                drop: function( event, ui ) {

                    var originalPos = {
                        stackid : ui.helper.data('id'),
                        stackHeight : ui.helper.data('stack'),
                        stackRow : ui.helper.data('row'),
                        stackCol : ui.helper.data('col')
                    }

                    var newPos = {
                        stackid : $(this).data('id'),
                        stackHeight : $(this).data('stack'),
                        stackRow : $(this).data('row'),
                        stackCol :$(this).data('col')
                    }

                    console.log(originalPos);
                    console.log(newPos);

                    //remove the small class if it exists...
                    $('.row-view').removeClass('temp-hide');
                    //show the background if its not shown..
                    $('.black-out').show();
                    //clear the row information
                    $('.row-information').html('');                
                    //regen the row.
                    _this.loadRows(rowNumber, stackInfo.id);
                    hideDropZone();
                }
            });
        },300);
            
    }

    function showDropZone(){
        $('.holding-zone').show();
        setTimeout(function(){
            $('.holding-zone').droppable({
                hoverClass: "drop-hover",        
                drop: function (event, ui) {
                }
            });  
        },300);      
    }

    function hideDropZone(){
        if ($('.holding-zone .container-doors').length === 0) {
            $('.holding-zone').hide();
        }
    }

    return {
        loadRows : loadRows,
        doAnimation : doAnimation
    }
});