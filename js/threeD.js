angular.module('yard-management')
.factory('threeD', function() {
    
    function start(selectedContainer){
        var container, stats;
        var camera, controls, scene, renderer;
        var pickingData = [], pickingTexture, pickingScene;
        var objects = [];
        var highlightBox;

        var mouse = new THREE.Vector2();
        var offset = new THREE.Vector3( 10, 10, 10 );

        init(selectedContainer);
        animate();
    }
    
    function init(selectedContainer) {

        container = document.getElementById( "container" );

        camera = new THREE.PerspectiveCamera( 10, window.innerWidth / window.innerHeight, 2, 10000 );
        camera.position.z = 2600;
        camera.position.y = 500;
        camera.position.x = 900;

        renderer = new THREE.WebGLRenderer( { antialias: true } );
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );

        
        
        scene = new THREE.Scene();
        scene.background = new THREE.Color( 0xffffff );

        pickingScene = new THREE.Scene();
        pickingTexture = new THREE.WebGLRenderTarget( window.innerWidth, window.innerHeight );
        pickingTexture.texture.minFilter = THREE.LinearFilter;

        scene.add( new THREE.AmbientLight( 0x555555 ) );

        var light = new THREE.SpotLight( 0xffffff, 31.5 );
        light.castShadow = true;
        light.position.set( 1500, 1500, 1500 );
        scene.add( light );

        var color = new THREE.Color();
        

        var geometry = new THREE.Geometry();


        function applyVertexColors( g, c ) {

            g.faces.forEach( function( f ) {

                var n = ( f instanceof THREE.Face3 ) ? 3 : 4;

                for( var j = 0; j < n; j ++ ) {

                    f.vertexColors[ j ] = c;

                }

            } );

        }

        var geom = new THREE.BoxGeometry( 1, 1, 1 );

        var matrix = new THREE.Matrix4();
        var quaternion = new THREE.Quaternion();

        //pre-defined width and length
        var length = 150;
        var width = 50;
        //material array
        var material = [];

        if (selectedContainer.size === 20) {
            length = 90;
        }

        
        var loader = new THREE.TextureLoader();
        var side = loader.load( '/img/side.jpg', render );
        var doors = loader.load('/img/doors.jpg', render );
        var roof = loader.load( '/img/roof.jpg', render );


        if (selectedContainer.orientation === "horizontal") {
            length = [width, width = length][0];

            material.push(new THREE.MeshBasicMaterial( {map: doors,vertexColors: THREE.VertexColors,opacity : 1}));
            material.push(new THREE.MeshBasicMaterial( {map: doors,vertexColors: THREE.VertexColors,opacity : 1}));
            material.push(new THREE.MeshBasicMaterial( {map: side,vertexColors: THREE.VertexColors,opacity : 1}));            
            material.push(new THREE.MeshBasicMaterial( {map: side, vertexColors: THREE.VertexColors, opacity : 1}));
            material.push(new THREE.MeshBasicMaterial( {map: side,vertexColors: THREE.VertexColors, opacity : 1}));            
            material.push(new THREE.MeshBasicMaterial( {map: side,vertexColors: THREE.VertexColors,opacity : 1}));
            
        } else {
            material.push(new THREE.MeshBasicMaterial( {map: side, vertexColors: THREE.VertexColors, opacity : 1}));
            material.push(new THREE.MeshBasicMaterial( {map: side,vertexColors: THREE.VertexColors,opacity : 1}));
            material.push(new THREE.MeshBasicMaterial( {map: roof,vertexColors: THREE.VertexColors,opacity : 1}));
            material.push(new THREE.MeshBasicMaterial( {map: roof,vertexColors: THREE.VertexColors,opacity : 1}));
            material.push(new THREE.MeshBasicMaterial( {map: doors,vertexColors: THREE.VertexColors,opacity : 1}));
            material.push(new THREE.MeshBasicMaterial( {map: doors,vertexColors: THREE.VertexColors,opacity : 1}));
        }
        
        var rowArray = [];
        var colArray = [];

        //iterate over the rows and stacks.
        for ( var z = 0; z < selectedContainer.rows; z ++ ) {
            for ( var x = 0; x < selectedContainer.cols; x ++ ) {     
                for ( var y = 0; y < selectedContainer.tiers; y ++ ) {              
                        
                    var blank = Math.floor(Math.random() * 10) + 1  
                    if (blank === 5) {
                        rowArray.push(x)
                        colArray.push(z)
                    }

                    if (rowArray.indexOf(x) === -1 || colArray.indexOf(z) === -1) {

                        var position = new THREE.Vector3();
                        position.x = x * width - 100;
                        position.y = y * 50 - 100;
                        position.z = z * length - 400;

                        var rotation = new THREE.Euler();
                    

                        var scale = new THREE.Vector3();
                        scale.x = width - 5;
                        scale.y = 50;
                        scale.z = length - 5;


                        quaternion.setFromEuler( rotation, false );
                        matrix.compose( position, quaternion, scale );

                        // give the geom's vertices a random color, to be displayed

                        applyVertexColors( geom, color.setHex( Math.random() * 0xffffff ) );

                        geometry.merge( geom, matrix );
                        

                        
                    } 


                }
                
            }
        }

        var drawnObject = new THREE.Mesh( geometry, material );
        scene.add( drawnObject );


        //apend the blocks to the container
        container.appendChild( renderer.domElement );

        //setup mouse controls for movements
        controls = new THREE.TrackballControls(camera, renderer.domElement);   
        controls.rotateSpeed = 1.0;
        controls.zoomSpeed = 1.2;
        controls.panSpeed = 0.3;
        controls.noZoom = false;
        controls.noPan = false;
        controls.staticMoving = false;
        controls.dynamicDampingFactor = 0.3;

        
        //event listener
        // renderer.domElement.addEventListener( 'mousemove', onMouseMove );

        

        
    }

    function animate() {
        requestAnimationFrame( animate );
        render();
    }



    function render() {
        controls.update();
        renderer.setSize($(container).width(), $(window).height() - 200);
        renderer.render( scene, camera );
        
    }
    return {
        start : start
    }

});
           