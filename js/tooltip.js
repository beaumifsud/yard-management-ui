angular.module('yard-management')
.factory('tooltip', function(yardManagementApi) {

    var tooltipTimer;

    
    function hide(){
        $('.tooltip-container').hide();
    }

    function show(colInfo,data,theContainer) {
        
        //get the stack
        var theStack = theContainer.closest('.stack');
        //get the row for height position
        var theElement = $('#'+colInfo.id+' .stack-row:nth-child('+(colInfo.row + 1)+')');
        //show the data
        var theData = "Row : "+colInfo.row + " Column : "+colInfo.col+"<br>";

        //dummy amount of hovered stack
        var stackTiers = parseInt(theContainer.text().split('/')[0]);

        if (stackTiers === 0 ) {
            theData = theData + "No Containers in this Stack.";
        } else {
            $.each(data,function(i,v){
                //dummy stack tiers
                if (i < stackTiers) {
                    theData = theData + "Tier " +(colInfo.tiers - i)+' - ' + this.containerNumber + ' ' + this.ISOCode + '<br>';  
                }
            });
        }

        var theTop = theStack.position().top - 30 - $('.tooltip-container').height() + theElement.position().top
        var theLeft = theStack.position().left - ($('.tooltip-container').width() / 2 - theContainer.width() / 2) + theContainer.position().left

        $('.tooltip-container')
        .show()
        .css({
            top : theTop,
            left : theLeft
        });
        
        clearTimeout(tooltipTimer);

        // imitate api call.
        tooltipTimer = setTimeout(function(){
            $('.tooltip-container .tooltip-content').html(theData)    

            theTop = theStack.position().top - 30 - $('.tooltip-container').height() + theElement.position().top
            theLeft = theStack.position().left - ($('.tooltip-container').width() / 2 - theContainer.width() / 2) + theContainer.position().left

            $('.tooltip-container')
            .css({
                top : theTop,
                left : theLeft
            });
        },1100);
    }

    function blank(){
        $('.tooltip-container .tooltip-content').html('<i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>').show();
    }

    return {
        hide : hide,
        show : show,
        blank : blank
    }
});