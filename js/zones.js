angular.module('yard-management')
.factory('zones', function(yardManagementApi) {
    
    function init() {

        var container = $('.ym-main-container .zone-filter-items');

        if (container.hasClass('d-none')) {
            loadFilterZones(container);
        } else {
            hideFilterZones(container)
        }
    }

    function hideFilterZones(container){
        container.addClass('d-none');

        $('.ym-main-container').off('click',function(){});
    }

    function showCreateZonesModal(){
        $('#createZones').modal(
            {backdrop: 'static', keyboard: false}
        );
    }
    
    function applFilters(){

        if ($('.ym-main-container .zone-filter-item input:checked').length > 0){
            $('.ym-main-container .stack').addClass('faded');
            $('.ym-main-container .zone-filter-item input:checked').each(function(){
                console.log($(this).val());
                $('.ym-main-container .stack[data-zone="'+$(this).val()+'"]').removeClass('faded');
            });
        } else {
            $('.ym-main-container .stack').removeClass('faded');
        }
    }    

    function saveZone(zoneId){

        //generate a guid if the zoneid = null;
        var zoneGuid; 
        if (zoneId) {
            //get the current zone id
            zoneGuid = zoneId;
        } else {
            //generate a new id
            zoneGuid = require("ym").getGuid();
        }

        //collect the zone info for api
        zoneInfo = {
            id : zoneGuid,
            name : $('#zoneName').val(),
            description : $('#zoneDescription').val(),
            rules : []
        }

        $('#createZones .stack-rule-inner-container').each(function(){
            var rule = $('.rule-category', this).val();
            var ruleOperator = $('.rule-operator', this).val();
            var rulesValues = $(".chosen",this).chosen().val();

            zoneInfo.rules.push({
                "rule" : rule,
                "ruleOperator":ruleOperator,
                "rulesValues":rulesValues
            });
            
        })
        
        console.log(zoneInfo);
        

        //do api
        yardManagementApi.saveZone(zoneInfo)
        .then(function( data ) {
        })
        .catch(function(response) {
            alert('COMPLETE API CALL: ' + response.responseText);
        })
        .finally(function() {
            //hide modal
            $('#createZones').modal('hide');
            //clear zones to we get them again on clikc.
            $('.zone-filter-items').html('');
        });
        
    }

    function loadFilterZones(container){

        $('.ym-main-container').on('click',function(e){

            if (!container.is(e.target) && container.has(e.target).length === 0) 
            {
                hideFilterZones(container);            
            }
        });

        var initButton = $('.ym-main-container .filter-zones');

        //check if its already here.
        if (container.find('input').length <= 0) {
            yardManagementApi.getZones('userid')
            .then(function(data){
                
                var content = ""
                $.each(data,function(){
                    content = content + "<div class='zone-filter-item'> <label><input type='checkbox' name='zone-filter' value="+this.id+" />"+this.name+"</label></div>";
                });
                
                container.html(content);
                
            });
        }
        container.removeClass('d-none');
        container.css({
            top : initButton.offset().top + initButton.height(),
            left : initButton.position().left,
        });
    }

    return {
        init : init,
        applFilters : applFilters,
        showCreateZonesModal : showCreateZonesModal,
        saveZone : saveZone
    }
});