
angular.module('yard-management')
.service('YardManagementService', function(tooltip, _stacks, threeD, allThreeD, yardGridSize, yardManagementApi, rows, selectedContainer, zones) {

    var gridSize = yardGridSize;
    var gridIncrements = 5;
    var startMove = false;
    var selectedContainer;
    var gridContainer = '.yard-layout';

    // init funtion called on load.
    function init($elem) {
        // document bindings
        initBindings($elem)
        // hide the tooltip for later use.
        tooltip.hide();
        // load the intitial stacks
        _stacks.loadStacks();

        // REMOVE WHEN LIVE
        // $('.edit-state.open').click();

        //set the init grid size
        $(gridContainer).css({'background-size' : gridSize+'px '+gridSize+'px'});

        
        // this is to clean up index..
        //replace with actual code.
        $.get('/modals/stack.html',function(data){$('.ym-main-container').append(data);});
        $.get('/modals/view3d.html',function(data){$('.ym-main-container').append(data);});
        $.get('/modals/reference.html',function(data){$('.ym-main-container').append(data);});
        $.get('/modals/zones.html',function(data){$('.ym-main-container').append(data);});

    }

    function initBindings($elem){
        // zoomin
        console.log($($elem));
        $($elem).on('click','.zoom-in',function(){
            setGridSize(1);
        })
        // zoom out 
        .on('click','.zoom-out',function(){
            setGridSize(0);
        })
        //edit layout enabled / disabled
        .on('click','.edit-state',function(){
            $('body').toggleClass('can-edit');
        })
        //load zones
        .on('click','.filter-zones',function(){
            zones.init();            
        })
        .on('click','.stack-object-edit',function(){
            _stacks.editStacks();
        })
        //load zones input
        .on('change','.zone-filter-item input',function(){
            zones.applFilters();            
        })
        //show zone create input
        .on('click','.zones-create',function(){
            zones.showCreateZonesModal();
        })
        //save the zone from the modal
        .on('click','.create-new-zone',function(){
            zones.saveZone();
        })
        //edit modal
        .on('click','.edit-mode',function(){
            _stacks.editStack($(this).attr('id'));
        })

        //show grid
        .on('change','#showGrid',function(){
            $(this).prop("checked") ? $(gridContainer).addClass('show-grid') : $(gridContainer).removeClass('show-grid');
        })
        //show roads
        .on('change','#showReference',function(){
            $(this).prop("checked") ? $('.object').removeClass('hide-object') : $('.object').addClass('hide-object');
        })
        //show tiers
        .on('change','#showTiers',function(){
            $(this).prop("checked") ? $('.stack-container').removeClass('hide-tiers') : $('.stack-container').addClass('hide-tiers');
        })
        //edit layout enabled / disabled
        .on('click','.reference-create',function(){
            _stacks.showReferenceModal();
        })
        .on('click','.view-3d',function(){
                
            $('#view3dstack').modal(
                {backdrop: 'static', keyboard: false}
            );
            $('#view3dstack').on('hidden.bs.modal',function(){
                $('#container').empty();
            })
            

            threeD.start(selectedContainer.get());
            
            
        })
        .on('click','.all3d',function(){
            $('#view3dstack').modal(
                {backdrop: 'static', keyboard: false}
            );
            $('#view3dstack').on('hidden.bs.modal',function(){
                $('#container').empty();
            })
            yardManagementApi.getStacks().then(function(stacks){
                console.log('x')
                console.log(stacks);
                allThreeD.start(stacks);
                
            });           
            
        })
        // hover container stack api call.
        .on('mouseenter','.stack:not(.ui-draggable):not(.edit-mode) .stack-container',function(){
            var theContainer = $(this);
            
            var colInfo = {
                'row' : theContainer.data('row'),
                'col' : theContainer.data('col'),
                'id' : theContainer.closest('.stack').attr('id'),
                'tiers' : parseInt(theContainer.closest('.stack').data('tiers'))
            }

            tooltip.blank(theContainer);

            //check if the moving is on, if so dont do stuff.
            if (!startMove) {
                
                yardManagementApi.tooltip(colInfo)
                .then(function(data){
                    tooltip.show(colInfo,data,theContainer)
                })
            }
        })
        //mouse leave hide tooltip
        .on('mouseleave','.stack-container',function(){
            tooltip.hide();
        })
        //show details of stack row
        .on('click','.stack:not(.ui-draggable):not(.edit-mode) .stack-container',function(){            

            var rowNumber = $(this).data('row');
            var stack = $(this).closest('.stack')
            var row = $(this).parent();

            var stackInfo = {
                id : stack.attr('id'),
                cols : stack.data('cols'),
                orientation : stack.data('pos'),
                rows : stack.data('rows'),
                tiers : stack.data('tiers'),
                size : stack.data('size')
            }

            selectedContainer.set(stackInfo)
            
            $('.row-view').addClass('show-info');
            row.addClass('row-selected');
            $('.black-out').removeClass('d-none').fadeOut(0).fadeIn(500).on('click',function(){
                hideRowInformation();
            });
            rows.doAnimation(row);
            rows.loadRows(rowNumber);

        })
        //create container modal
        .on('click','.block-create',function(){
            _stacks.showModal();
        })
        //create bay modal
        .on('click','.bay-create',function(){
            $('#createBayModal').modal('show'); 
            var randomColor = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
            $("#colorpickerCreateBay").spectrum({color: randomColor});   
        })
        
        //build the bay
        .on('click','.build-bay',function(){
            
            var bayInfo = {
                id : Math.floor(Date.now() / 1000),
                rows : $('#createBayModal #rowsCreate').val(),
                zone : $('#createBayModal #zoneCreate').val(),
                columns : $('#createBayModal #colsCreate').val(),
                size : $('#createBayModal input[name=sizeCreate]:checked').val(),
                orientation : $('#createBayModal input[name=orienCreate]:checked').val(),
                tiers : 1,
                primaryColor : $('#createBayModal  #colorpickerCreateBay').spectrum('get').toHexString(),
                type : 'Bay'
            }

            console.log(bayInfo)

            $('#createBayModal').modal('hide');            
            _stacks.createStacks(bayInfo, true);
        })
        
        //zones  modal
        .on('click','.zones-create',function(){
            //crate stack modal
            $('#manageZones').modal('show');    
        })
        //seach parameters dropdown
        .on('click','.main-search .dropdown-item',function(e){
            //crate stack modal
            e.preventDefault();
            var selectedItem = $(this).text();
            $('.search-type').text(selectedItem)
        })
        //build reference
        .on('click','.build-reference',function(){
            var referenceInfo = {
                id : Math.floor(Date.now() / 1000),
                primaryColor : $('#colorpickerCreateReference').spectrum('get').toRgbString(),
                shape : $('input[name=shape]:checked').val(),
                size : {
                    height : 5,
                    width : 5
                },
                label : {
                    text : $('#labelText').val(),
                    orientation : $('input[name=labelOrientation]:checked').val()
                }
            }

            $('#createReferenceModal').modal('hide');            
            _stacks.createObject(referenceInfo, true);
        })
        //build stack from in the modal
        .on('click','.build-stack',function(){
            //create stack with attributes

            var stackInfo = {
                id : Math.floor(Date.now() / 1000),
                rows : $('#createStackModal #rowsCreate').val(),
                zone : $('#createStackModal #zoneCreate').val(),
                columns : $('#createStackModal #colsCreate').val(),
                size : $('#createStackModal input[name=sizeCreate]:checked').val(),
                orientation : $('#createStackModal input[name=orienCreate]:checked').val(),
                tiers : $('#createStackModal #tiersCreate').val(),
                primaryColor : $('#createStackModal  #colorpickerCreate').spectrum('get').toHexString(),
            }

            $('#createStackModal').modal('hide');            
            _stacks.createStacks(stackInfo, true);
        })
        //move the stacks.
        .on('click','.stack-object-move',function(){
            if (startMove === false) {

                var topConstrain = $('.ym-main-container').position().top;
                var leftConstrain = $('.ym-main-container').position().left;

                $( ".object" ).resizable({
                    grid: gridSize,
                    handles: "n, e, s, w",
                    stop: function(event, ui) {                       
                        _stacks.newPosition(ui.helper,true)
                    }
                });

                $(".stack, .object").draggable({ 
                    grid: [gridSize, gridSize],                    
                    start: function(event, ui) {
                    },
                    drag: function(event, ui) {                       
                    },
                    stop: function(event, ui) {                       
                        _stacks.newPosition(ui.helper,false)
                    }
                });

                startMove = true;
            } else {
                $('.stack, .object').draggable("destroy");
                $('.object').resizable("destroy");
                startMove = false;
            }
        })
        //search
        .on('keyup','#search',function(){
            $('.search-found').removeClass('search-found');
            var searchValue = $(this).val().trim();
            if (searchValue) {
                yardManagementApi.search(searchValue)
                .then(function(data){
                    $.each(data,function(){
                        $('.stack[id='+this.id+']').find('.stack-container[data-row='+this.row+'][data-col='+this.col+']').addClass('search-found');
                    });
                });
            }
        }).on('click','[data-hide], [data-show]',function(){
            var hide = $(this).data('hide');
            var show = $(this).data('show');
            if (hide) {$(hide).hide();}
            if (show) {$(show).show();}
        })
        .on('click','.add-rule',function(){
            var modal = $(this).closest('.modal').attr('id');
                        
            var randomNumber = Math.floor((Math.random() * 9999) + 1);
            $('#'+modal+' .hidden-template').clone().removeClass('hidden-template d-none').addClass('stack-rule-inner-container row').attr('id',randomNumber).appendTo('#'+modal+' .stack-rules-container');
            $('#'+modal+' #'+randomNumber+' .chosen').chosen();
        })
        // do full screen
        .on('click','.full-screen',function(){
            requestFullScreen();
        })
        .on('click','.remove-row',function(){
            $(this).closest('.stack-rule-inner-container').remove();            
        })
        .on('click','.save-block-position',function(){
        
            //set the new elements position
            var element = $(this).prev('.ui-draggable');
            
            if (element.hasClass('object')){
                //if required to save width and height
                _stacks.newPosition(element,true);
            } else {
                _stacks.newPosition(element,false);
            }
            
            $(this).remove();
            $('.ui-draggable').draggable('destroy');
            $('.ui-resizable').resizable('destroy');
        });
    }
    
    function requestFullScreen() {

        var el = document.getElementsByClassName('ym-main-container')[0];
        var rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;

        if (el.classList.contains('full-screen')) {
            el.classList.remove("full-screen");
            el = document;            
            rfs = el.exitFullscreen || el.mozCancelFullScreen || el.webkitExitFullscreen || el.msExitFullscreen;            
        } else {
            el.classList.add('full-screen');
        }

        if(typeof rfs!="undefined" && rfs){
            rfs.call(el);
        } else if (typeof window.ActiveXObject!="undefined"){
        // for Internet Explorer
        var wscript = new ActiveXObject("WScript.Shell");
            if (wscript!=null) {
                wscript.SendKeys("{F11}");
            }
        }
    }

    function getGridSize(){
        return gridSize;
    }

    function hideRowInformation(){
        $('.row-view').removeClass('show-info')
        setTimeout(function(){
            $('.row-view .row-information').html('');
        },300)
        $('.row-selected').removeClass('row-selected');
        $('.black-out').fadeOut(500,function(){
            $(this).addClass('d-none')
        }).off('click',function(){});
    }

    function setGridSize(size){

        //logic to change grid size
        if (size === 1) {
            if (gridSize !== 60) {
                gridSize += 5;
            }
        } else {
            if (gridSize !== 10) {
                gridSize -= 5;
            }
        }
        //round number to 1 decimal place.
        scale = Math.round( gridSize * 10 ) / 10;

        //set the scale of the yard layout
        //this keeps proportions etc
        $(gridContainer).css({'background-size' : scale+'px '+scale+'px'});

        $('.object').each(function(){
            var newHeight = $(this).data('height') * gridSize + 'px';
            var newWidth = $(this).data('width') * gridSize + 'px';
            var newTop = $(this).data('top') * gridSize + 'px';
            var newLeft = $(this).data('left') * gridSize + 'px';

            $(this).css({
                height : newHeight,
                width : newWidth,
                top : newTop,
                left : newLeft,
            })
        })

        $('.stack').each(function(){
            
            if (parseInt($(this).data('size')) === 20) {

                var gridWidth = gridSize;
                var gridHeight = gridSize;
    
            } else {
    
                if ($(this).data('pos') === "vertical") {
                    var gridHeight = gridSize*2;
                    var gridWidth = gridSize;
                } else {
                    var gridHeight = gridSize;
                    var gridWidth = gridSize*2;
                }
    
            }

            var newHeight = $(this).data('rows') * gridHeight + 'px';
            var newWidth = $(this).data('cols') * gridWidth + 'px';
            var newTop = $(this).data('top') * gridSize + 'px';
            var newLeft = $(this).data('left') * gridSize + 'px';

            $(this).css({
                height : newHeight,
                width : newWidth,
                top : newTop,
                left : newLeft,
            })
        })
        
        console.log(gridSize)

        
    }

    function getGuid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
      }
    
    return {
        //public funtions
        init : init,
        getGridSize : getGridSize,
        getGuid : getGuid
    }

})