angular.module('yard-management')
.factory('_stacks', function(yardManagementApi, yardGridSize) {

    var gridSize;
    var editMode = false;
    var appendContainer = '.yard-layout'

    function loadStacks(){

        gridSize = yardGridSize;
        
        yardManagementApi.getStacks('userid')
        .then( function(data){
            console.log(data);
            //generate the track infromation
            processJSON(data, false);
        });    
    }

    function processJSON(stackInfo, newStack) {

        $.each(stackInfo,function(i,v){
            switch(this.type) {
                case 'stack':
                    createStacks(this, false);
                    break;
                case 'object':
                    createObject(this, false);
                    break;
                case 'bay':
                    createStacks(this, false);
                    break;
                default:
                    break
            }
        })
    }

    function createObject(objectInfo, newObject){

        gridSize = yardGridSize;
        

        if (newObject) {
            var posLeft = Math.round(($(window).width()/2)/gridSize);
            var posTop = Math.round(($(window).height()/2)/gridSize);
        } else {
            posLeft = objectInfo.position.left;    
            posTop = objectInfo.position.top;    
        } 

        var draggableStack = "";

        if (newObject) {
            draggableStack = "start-drag";
            $('.black-out').removeClass('d-none').fadeOut(0).fadeIn(500);
        }

        $('<div class="object '+draggableStack+' '+objectInfo.label.orientation+' '+ objectInfo.shape +'" data-width="'+objectInfo.size.width+'" data-height="'+objectInfo.size.height+'" data-left="'+posLeft+'" data-top="'+posTop+'" id="'+objectInfo.id+'"  style="left:'+posLeft * gridSize+'px; top:'+posTop * gridSize+'px; height:'+objectInfo.size.height * gridSize+'px; width:'+objectInfo.size.width * gridSize+'px; background-color:'+objectInfo.primaryColor+'"><span>'+objectInfo.label.text+'</span></div>').appendTo(appendContainer);

        if (newObject) {
            $("#"+objectInfo.id).draggable({ 
                grid: [gridSize, gridSize],
                start: function(event, ui) {
                    startMove(objectInfo.id);
                },
                stop : function(event,ui){
                    stopMove(ui);
                }
            })

            $("#"+objectInfo.id).resizable({
                grid: gridSize,
                handles: "n, e, s, w",
                start: function(event, ui) {
                    startMove(objectInfo.id);
                },
                stop : function(event,ui){
                    stopMove(ui);
                }
            });
            
        }      


    }

    function startMove(theId){
        $("#"+theId).removeClass('start-drag');
        $('.black-out').fadeOut(200);
        $('.save-block-position').remove();
    }

    function stopMove(ui){
        
        var elem = ui.helper;
        var roundLeft = gridSize*Math.round(elem.position().left/gridSize);
        var roundTop = gridSize*Math.round(elem.position().top/gridSize);
        elem.css({left: roundLeft,top: roundTop});
        
        $('<div class="save-block-position" style="position:absolute; top:'+(roundTop + elem.height()) +'px; left:'+roundLeft+'px; width:'+elem.width()+'px">Save Position</div>').appendTo(appendContainer);
    }

    function createStacks(stackInfo, newStack) {

        if (newStack) {
            var posLeft = Math.round(($(window).width()/2)/gridSize);
            var posTop = Math.round(($(window).height()/2)/gridSize);
        } else {
            posLeft = stackInfo.position.left;    
            posTop = stackInfo.position.top;    
        } 

        var draggableStack = "";
        if (newStack) {
            draggableStack = "start-drag";
            $('.black-out').removeClass('d-none').fadeOut(0).fadeIn(500);
        }

        $('<div class="stack '+draggableStack+'" data-type="'+stackInfo.type+'" id="'+stackInfo.id+'" data-zone="'+stackInfo.zone+'" data-left="'+posLeft+'" data-top="'+posTop+'" data-rows="'+stackInfo.rows+'" data-tiers="'+stackInfo.tiers+'" data-cols="'+stackInfo.columns+'" data-pos="'+stackInfo.orientation+'" data-size="'+stackInfo.size+'" style="left:'+posLeft*gridSize+'px; top:'+posTop*gridSize+'px; background-color:'+stackInfo.primaryColor+'; border-color:'+stackInfo.primaryColor+'" />').appendTo(appendContainer);

        stackContents(stackInfo);

        if (newStack) {
            $("#"+stackInfo.id).draggable({ 
                grid: [gridSize, gridSize],
                start: function(event, ui) {
                    $("#"+stackInfo.id).removeClass('start-drag');
                    $('.black-out').fadeOut(200);
                    $('.save-block-position').remove();
                },
                drag: function(event, ui) {},                    
                stop : function(event,ui){
                    //make it snap to the grid.
                    var elem = ui.helper;
                    var roundLeft = gridSize*Math.round(elem.position().left/gridSize);
                    var roundTop = gridSize*Math.round(elem.position().top/gridSize);
                    elem.css({left: roundLeft,top: roundTop});
                    
                    $('<div class="save-block-position" style="position:absolute; top:'+(roundTop + elem.height()) +'px; left:'+roundLeft+'px; width:'+elem.width()+'px">Save Position</div>').appendTo(appendContainer);
                }
            });
        }
    }

    function stackContents(stackInfo){
        var capacityCounter = 0;
        var contentString = "";

        for(var rowsCounter = 0; rowsCounter < stackInfo.rows; rowsCounter++){
            
            contentString = contentString + "<div class='stack-row' data-row='"+rowsCounter+"' style='border-color:"+stackInfo.primaryColor+";background-color:"+stackInfo.primaryColor+"'>";
            
            for(var colsCounter = 0; colsCounter < stackInfo.columns; colsCounter++){
                //set opac on bg
                var capacityValue = 0;
                if (stackInfo.capacity) {
                    var capacityValue = stackInfo.capacity[capacityCounter];
                } 
                var opacity = (1 - (capacityValue / stackInfo.tiers) * .8);
                var shadow = (capacityValue * stackInfo.tiers) * .5;
                
                contentString = contentString + "<div data-row='"+rowsCounter+"' data-col='"+colsCounter+"' class='stack-container hide-tiers' style='border-color:"+stackInfo.primaryColor+"; box-shadow: "+(capacityValue)+"px "+(capacityValue)+"px 2px 2px rgba(0,0,0,0.3); z-index:"+capacityValue+"'>";
                contentString = contentString +     "<div class='capacity-amount' style='background-color:rgba(255,255,255,"+opacity+"); border-color:"+stackInfo.primaryColor+"'><span>"+ capacityValue + '<i class="tiers">/' + stackInfo.tiers + "</i></span></div>";
                contentString = contentString + "</div>";
                
                capacityCounter++;
            }

            contentString = contentString + "</div>";

        };

        $(contentString).appendTo($('#'+stackInfo.id));      

        //logic for sizing.
        if (parseInt(stackInfo.size) === 20) {

            var gridWidth = gridSize;
            var gridHeight = gridSize;

        } else {

            if (stackInfo.orientation === "vertical") {
                var gridHeight = gridSize*2;
                var gridWidth = gridSize;
            } else {
                var gridHeight = gridSize;
                var gridWidth = gridSize*2;
            }

        }
        
        //set the css of the container stack
        $('#'+stackInfo.id).css({
            'height': (stackInfo.rows * gridHeight),
            'width': (stackInfo.columns * gridWidth),
        });
    }

    function showModal(){

        //crate stack modal
        $('#createStackModal').modal('show');

        var randomColor = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        $("#colorpickerCreate").spectrum({color: randomColor});

    }

    function showReferenceModal() {
        //crate stack modal
        $('#createReferenceModal').modal(
            {backdrop: 'static', keyboard: false}
        );

        var randomColor = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        $("#colorpickerCreateReference").spectrum({
            color: randomColor,    
            showAlpha: true,
        });
    }

    function editStacks(){
        if (editMode) {
            $('.stack, .object').removeClass('edit-mode');
            editMode = false;
        } else {
            $('.stack, .object').addClass('edit-mode');
            editMode = true;
        }
    }

    // edit individual stack or object
    function editStack(stackId){
        alert(stackId)
    }

    function newPosition(element,sizeChange){
        
        var newTop = element.position().top / gridSize;
        var newLeft = element.position().left / gridSize;

        element.data('top',newTop);
        element.data('left',newLeft);

        var newWidth = false;
        var newHeight = false;

        if (sizeChange) {
            newWidth = element.width() / gridSize;
            newHeight = element.height() / gridSize;
            element.data('height',newHeight)
            element.data('width',newWidth)
        }

        yardManagementApi.newPosition(element.attr("id"),newTop,newLeft,newHeight,newWidth)
        .then(function( data ) {

        }).catch(function(response) {
            //do fail
        }).finally(function() {
            //do feedback
        });
        

    }
    
    return {
        createStacks : createStacks,
        showReferenceModal : showReferenceModal,
        stackContents : stackContents,
        showModal : showModal,
        loadStacks : loadStacks,
        createObject : createObject,
        editStacks : editStacks,
        editStack : editStack,
        newPosition : newPosition
    }
})