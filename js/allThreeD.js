angular.module('yard-management')
    .factory('allThreeD', function () {

        var controls;
        var SHADOW_MAP_WIDTH = 2048, SHADOW_MAP_HEIGHT = 1024;
        var lightShadowMapViewer;

        function start(allStacks) {
            var container, stats, scene, renderer;
            var pickingData = [], pickingTexture, pickingScene;
            var objects = [];
            var highlightBox;

            var mouse = new THREE.Vector2();
            var offset = new THREE.Vector3(1, 1, 1);

            init(allStacks);
            animate();
        }

        function init(allStacks) {

            container = document.getElementById("container");

            

            renderer = new THREE.WebGLRenderer({ antialias: true });
            renderer.setPixelRatio(window.devicePixelRatio);
            renderer.setSize(window.innerWidth, window.innerHeight);



            scene = new THREE.Scene();
            scene.background = new THREE.Color(0xffffff);

            scene.add( new THREE.AmbientLight( 0xcccccc ) );

            var color = new THREE.Color();


            var geometry = new THREE.Geometry();


            function applyVertexColors(g, c) {

                g.faces.forEach(function (f) {

                    var n = (f instanceof THREE.Face3) ? 3 : 4;

                    for (var j = 0; j < n; j++) {

                        f.vertexColors[j] = c;

                    }

                });

            }

            var geom = new THREE.BoxGeometry(1, 1, 1);

            var matrix = new THREE.Matrix4();
            var quaternion = new THREE.Quaternion();

            var loader = new THREE.TextureLoader();
            var side = loader.load('/img/side.jpg', render);
            var doors = loader.load('/img/doors.jpg', render);
            var roof = loader.load('/img/roof.jpg', render);
            
            var material;

            var geometryx = new THREE.PlaneBufferGeometry( 100, 100 );
            var planeMaterial = new THREE.MeshPhongMaterial( { color:0xffffff } );
            var ground = new THREE.Mesh( geometryx, planeMaterial );
            
            ground.position.set( 1000, -50, 1000 );
            ground.rotation.x = - Math.PI / 2;
            ground.scale.set( 50, 50, 50 );
            scene.add( ground );
            
            material = [];
            material.push(new THREE.MeshBasicMaterial({ map: side, vertexColors: THREE.VertexColors, opacity: 1 }));
            material.push(new THREE.MeshBasicMaterial({ map: side, vertexColors: THREE.VertexColors, opacity: 1 }));
            material.push(new THREE.MeshBasicMaterial({ map: roof, vertexColors: THREE.VertexColors, opacity: 1 }));
            material.push(new THREE.MeshBasicMaterial({ map: roof, vertexColors: THREE.VertexColors, opacity: 1 }));
            material.push(new THREE.MeshBasicMaterial({ map: doors, vertexColors: THREE.VertexColors, opacity: 1 }));
            material.push(new THREE.MeshBasicMaterial({ map: doors, vertexColors: THREE.VertexColors, opacity: 1 }));

            $.each(allStacks, function () {

                var rowArray = [];
                var colArray = [];

                var _this = this;

                if (_this.type == 'stack') {

                    var selectedContainer = {
                        id: _this.id,
                        orientation: _this.orientation,
                        cols: parseInt(_this.columns),
                        rows: parseInt(_this.rows),
                        tiers: parseInt(_this.tiers),
                        size: parseInt(_this.size),
                        top: parseInt(_this.position.top),
                        left: parseInt(_this.position.left)
                    };

                     //pre-defined width and length
                     var length = 150;
                     var width = 50;

                    if (selectedContainer.size === 20) {
                        length = 75;
                    }

                    
                    //material array
                
                if (selectedContainer.orientation === "horizontal") {
                    console.log('x');
                    length = [width, width = length][0];

                } else {
                    
                }

                   

                    //iterate over the rows and stacks.
                    for (var z = 0; z < selectedContainer.rows; z++) {
                        for (var x = 0; x < selectedContainer.cols; x++) {
                            for (var y = 0; y < selectedContainer.tiers; y++) {

                                var xx = parseInt((selectedContainer.left * 70) + x * width);
                                var zz = parseInt((selectedContainer.top * 70) + z * length);

                                var blank = Math.floor(Math.random() * 10) + 1
                                if (blank === 5) {
                                    rowArray.push(x)
                                    colArray.push(z)
                                }

                                if (rowArray.indexOf(x) === -1 || colArray.indexOf(z) === -1) {

                                    var position = new THREE.Vector3();
                                    position.x = xx;
                                    position.y = y * 50 ;
                                    position.z = zz;

                                    var rotation = new THREE.Euler();


                                    var scale = new THREE.Vector3();
                                    scale.x = width - 5;
                                    scale.y = 50;
                                    scale.z = length - 5;

                                    


                                    quaternion.setFromEuler(rotation, false);
                                    matrix.compose(position, quaternion, scale);

                                    // give the geom's vertices a random color, to be displayed

                                    applyVertexColors(geom, color.setHex(Math.random() * 0xffffff));

                                    geometry.merge(geom, matrix);



                                }


                            }

                        }
                    }



                } else if (_this.type === 'object') {
                    console.log(_this);
                }
            });

            

            var drawnObject = new THREE.Mesh(geometry, material);
            scene.add(drawnObject);

            //apend the blocks to the container
            container.appendChild(renderer.domElement);

            //setup mouse controls for movements
            
            //event listener
            // renderer.domElement.addEventListener( 'mousemove', onMouseMove );


            camera = new THREE.PerspectiveCamera(5, window.innerWidth / window.innerHeight, 2, 100000);
            camera.position.z = 8427;
            camera.position.y = 10780;
            camera.position.x = 10232;   
            camera.rotation.x = -0.9589059500889253;
            camera.rotation.y = -0.7743361574002802;
            camera.rotation.z = -0.5875670890997;

            controls = new THREE.TrackballControls(camera, renderer.domElement);
            controls.rotateSpeed = 1.0;
            controls.zoomSpeed = 1.2;
            controls.panSpeed = 0.3;
            controls.noZoom = false;
            controls.noPan = false;
            controls.staticMoving = false;
            controls.dynamicDampingFactor = 0.3;

            controls.update();

        }

        function animate() {
            requestAnimationFrame(animate);            
            
			renderer.render( scene, camera );            
            render();
        }



        function render() {

            controls.update();
            renderer.setSize($(container).width(), $(window).height() - 200);
            
            renderer.render(scene, camera);

        }
        return {
            start: start
        }

    });
